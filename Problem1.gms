*#######################################*
******** ASSIGNMENT 1, Problem 1 ********
************* Emilie Popp ***************
*#######################################*

*First I define the indices I will use in the following model of the problem.
SETS
i        Player i        /p1*p25/
j        Position j      /GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
k        Formation k     /f442, f352, f4312, f433, f343, f4321/

*I create two subsets of the players, one containing the quality players and
*one containing the strenght players.
q(i)     Quality player /p13, p20, p21, p22/
s(i)     Strenght player /p10, p12, p23/;

*I create two tables, the first d(k,j) corresponds to Table 1 in the problem
*formulation. The second m(i,j) corresponds to table 2 that assigns a fitness
*to each player depending on the role.
TABLE
d(k,j) The number of players required in each position j in formation k
         GK  CDF  LB  RB  CMF  LW  RW  OMF  CFW  SFW
f442     1   2    1   1   2    1   1   0    2    0
f352     1   3    0   0   3    1   1   0    1    1
f4312    1   2    1   1   3    0   0   1    2    0
f433     1   2    1   1   3    0   0   0    1    2
f343     1   3    0   0   2    1   1   0    1    2
f4321    1   2    1   1   3    0   0   2    1    0;

TABLE
m(i,j) The fitness of player pi if player pi plays position j
      GK  CDF  LB  RB  CMF  LW  RW  OMF  CFW  SFW
p1    10  0    0   0   0    0   0   0    0    0
p2    9   0    0   0   0    0   0   0    0    0
p3    8.5 0    0   0   0    0   0   0    0    0
p4    0   8    6   5   4    2   2   0    0    0
p5    0   9    7   3   2    0   2   0    0    0
p6    0   8    7   7   3    2   2   0    0    0
p7    0   6    8   8   0    6   6   0    0    0
p8    0   4    5   9   0    6   6   0    0    0
p9    0   5    9   4   0    7   2   0    0    0
p10   0   4    2   2   9    2   2   0    0    0
p11   0   3    1   1   8    1   1   4    0    0
p12   0   3    0   2   10   1   1   0    0    0
p13   0   0    0   0   7    0   0   10   6    0
p14   0   0    0   0   4    8   6   5    0    0
p15   0   0    0   0   4    6   9   6    0    0
p16   0   0    0   0   0    7   3   0    0    0
p17   0   0    0   0   3    0   9   0    0    0
p18   0   0    0   0   0    0   0   6    9    6
p19   0   0    0   0   0    0   0   5    8    7
p20   0   0    0   0   0    0   0   4    4    10
p21   0   0    0   0   0    0   0   3    9    9
p22   0   0    0   0   0    0   0   0    8    8
p23   0   3    1   1   8    4   3   5    0    0
p24   0   3    2   4   7    6   5   6    4    0
p25   0   4    2   2   6    7   5   2    2    0;

*I need three new variables in order to model the problem properly:
* - z is the objective, this is the variable we seek to maximize.
* - x(i,j) is used to model wheather player i is used in position j or not.
* - y(k) indicates what formation we choose. It is equal to one if we choose
*   formation k and zero otherwise.

VARIABLES
z        Total player-position fitness
x(i,j)   Binary variable equal to 1 if player i is on position j and 0 otherwise
y(k)     Binary variable indicating if we choose formation k or not;

* Since x and y are used as indicators telling wheather an event occur or not,
* they are binary variables.
BINARY VARIABLES x, y;

* The following five equations are used to model the problem.
EQUATIONS
reward                 Total player-position fitness
oneFormation           We can only choose one formation
minQuality             We need to employ at least one quality player
qualityStrength        We need at least one strengt player if we use 4 quality players
playerFormation(j)     How many player of each type is needed in each formation;

*This is the objective function. I sum over the player-fitness entries in the
*matrix m(i,j) times x(i,j). This means that I only sum over the entries in
*m(i,j) if I assign player i to position j.
reward..           z =e= sum((i,j), m(i,j)*x(i,j));

*I can only choose one formation so the sum of the y(k) has to be equal 1.
oneFormation..     sum(k, y(k)) =e= 1;

*I need to use at least one of the quality players. So the sum of x(i,j) where
*i is equat to 13, 20, 21 or 22 (the quality players) has to be at least one.
minQuality..       sum((q(i),j), x(i,j)) =g= 1;

*The number of quality players has to be less than the number of strenght
*players plus 3. This forces the number of strength players to be at least
*one if we use all four quality players.
qualityStrength..  sum((q(i),j), x(i,j)) =l= sum((s(i),j), x(i,j)) + 3;

*For each position j, the amount of players at that position in formation k
*has to be equal the number of players I choose at that position.
playerFormation(j)..  sum(k, y(k)*d(k,j)) =e= sum(i, x(i,j));

*I want to maximize z, the total player-position fitness.
MODEL Problem1 /all/;
SOLVE Problem1 USING mip MAXIMIZING z;

DISPLAY z.l, z.m, x.l, y.l;



