*#######################################*
******** ASSIGNMENT 1, Problem 3 ********
************* Emilie Popp ***************
*#######################################*

*I create the following indices I will use to model the problem.
SETS
i   Index of the ships /v1*v5/
j   Index of the route /Asia, ChinaPacific/
p   Index of the ports /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/

*I create two subsets consisting of ports that are incompatible.
w1(p) incompatible subset af p /Singapore, Osaka/
W2(p) incompatible subset af p /Incheon, Victoria/;

*I create the parameters as given in the problem text.
PARAMETERS
F(i)   The fixed cost associated with using ship i
                 /v1 65
                  v2 60
                  v3 92
                  v4 100
                  v5 110/
G(i)   The maximum amount of days ship i can sail a year
                 /v1 300
                  v2 250
                  v3 350
                  v4 330
                  v5 300/;
PARAMETER
D(p) The minimum amount of times we have to visit port p if we visit it at all
         /Singapore      15
          Incheon        18
          Shanghai       32
          Sydney         32
          Gladstone      45
          Dalian         32
          Osaka          15
          Victoria       18/;

*The following three tables are created in accordance with the problem text.
TABLE
C(i,j) The cost of ship i sailing route j
         Asia  ChinaPacific
v1       1.41    1.9
v2       3.0     1.5
v3       0.4     0.8
v4       0.5     0.7
v5       0.7     0.8;

TABLE
T(i,j) The number of days it takes ship i to complete route j
         Asia  ChinaPacific
v1       14.4    21.2
v2       13.0    20.7
v3       14.4    20.6
v4       13.0    19.2
v5       12.0    20.1;

TABLE
A(p,j) Indicator of wheather route j sails by port p
             Asia    ChinaPacific
Singapore      1         0
Incheon        1         0
Shanghai       1         1
Sydney         0         1
Gladstone      0         1
Dalian         1         1
Osaka          1         0
Victoria       0         1;

*The scalars are constants used in the model.
SCALARS
k        How many ports the company at least has to service  /5/

*In one of the constraints I need an upper bound.
M        Constant used to construct upperbpund in constraint  /1000/;

*I need four new variables in order to model the problem properly:
VARIABLES
z        The total yearly cost
x(i)     Binary variable equal to 1 if ship i sails and zero otherwise
q(p)     Binary variable equal to 1 if port p is visited and zero otherwise
y(i,j)   Integer variable indicating how many times ship i sails route j;

BINARY VARIABLES x, q;
INTEGER VARIABLE y;

*The following seven equations model our problem in order to minimize cost, z.
EQUATIONS
costs            The total costs
minPort          The minimum number of ports the company has to vistit
incompatible1    Takes into account that we cannot visit both Singapore and Osaka
incompatible2    Takes into account that we cannot visit both Incheon and Victoria
maxDays(i)       This constraint makes sure that ship i does not sail more than G(i) days a year
minVisit(p)      If we do visit port p we have to visit port p at least d(p) times
condition(i)     If y(ij) is positive then we need x(i) to be equal to 1;

*The objective is to minimize the cost. The cost consist of the sum of the fixed
*costs of ships that sails plus the cost of ship i sailing route j times the
*number of times ship i do sail route j.
costs..          z =e= sum(i, F(i)*x(i)) + sum((i,j),C(i,j)*y(i,j));

*The company is obliged by contract to visit at least k=5 ports.
minPort..        k =l= sum(p, q(p));

*The following two conditions ensures that I do not visit both ports in
*the incompatible sets.
incompatible1..  sum(w1(p), q(p)) =l= 1;
incompatible2..  sum(w2(p), q(p)) =l= 1;

*LHS: the amount of times ship i sails route j times the amount of days it
*takes to sail route j has to be smaller than or equal RHS: the maximum
*number of days ship i can sail a year.
maxDays(i)..     sum(j, y(i,j)*T(i,j)) =l= G(i);

*If ship i sail route j and visit port p on route j, then the company needst
*to vistit port p at least d(p) times. q(p) is equal to one if we vistit port
*p and zero otherwise.
minVisit(p)..    sum((i,j), y(i,j)*A(p,j)) =g= q(p)*D(p);

*If ship i sail route j y(i,j) times, then x(i) need to be one, since x(i)
*tells wheater ship i sails or not. This is in order to activate the fixed cost
*of using ship i. I use M=1000 as an upper bound.
condition(i)..   sum(j, y(i,j)) =l= M*x(i);

*I minimize the cost, z.
MODEL Problem3 /all/;
SOLVE Problem3 USING mip MINIMIZING z;

DISPLAY z.l, x.l, y.l, q.l;



