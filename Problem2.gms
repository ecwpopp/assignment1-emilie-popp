*#######################################*
******** ASSIGNMENT 1, Problem 2 ********
************* Emilie Popp ***************
*#######################################*

*I create the index i for the nodes and use the alias statement so I can refer
*to connected nodes without ambiguity. I set k as the index for the keys.
SET
i The number of nodes /1*8/;
alias(i,j);

SET
k The number of keys /1*30/;

*The constants used in the model are set as scalars.
SCALARS
nodeMemory       The amount of memory in one node in kB /700/
keyMemory        The amount of memory one key occupies in kB /186/
q    The minimum number of keys two nodes should have in common to establish direct connection /3/
t    The maximum nodes one key can be assigned to /4/;

*I need four new variables in order to model the problem properly:
VARIABLES
z        The total number of direct connections
y(i,k)   Binary variable equal to 1 if key k is assigned to node i and zero otherwise
x(i,j)   Binary variable equal to 1 if there is a direct connection between node i and node j and zero otherwise
c(i,j,k) Binary variable equal to 1 if node i and j both have key k assigned and zero otherwise;

*As decribed above, y, x and c are binary variables.
BINARY VARIABLES y, x, c;

*I need the following seven equations in order to assign keys to nodes in a way
*that maximizes the total number of direct connections.
EQUATIONS
connections      The total amount of direct connections
memory(i)        The total space the keys assigned to a node occupies cannot exceed the memory of the node
maxAssign(k)     The same key can be assigned to maximum 4  nodes

*Together condition1 and condition2 ensures that if two nodes have key k in
*common, then y(i,k) and y(j,k) will be 1.
condition1(i,j,k)     If node i an j both have key k assigned y will be 1
condition2(i,j,k)     If node i an j both have key k assigned y will be 1

*Together condition3 and condition4 ensures that if two nodes have 3 keys in
*common, then x(i,j) will be 1, and a direct connection is established
condition3(i,j)       xij is will be one if there is a direct connection between node i and j
condition4(i,j)       xij is will be one if there is a direct connection between node i and j;

*This is the objective function, I maximize the number of direct connections.
*The condition i<j ensures that I do not count both a connection from i to j
*and one from j to i. The condition i<j is used similarly in the other equations.
connections..    z =e= sum((i,j)$(ord(i) lt ord(j)), x(i,j));

*LHS: the sum of the keys assigned to node i times the memory they occupy has
*to be smaller than or equal the RHS: the memory of each node.
memory(i)..      sum(k, keyMemory*y(i,k)) =l= nodeMemory;

*LHS: The number of nodes that key k are assigned to has to be smaller than or
*equal 4.
maxAssign(k)..   sum(i, y(i,k)) =l= 4;

*If key k are assigned to both node i and j then c(i,j,k) is equal to 1.
*The following condition then ensures that both y(i,k) and y(j,k) will be 1.
condition1(i,j,k)..   (y(i,k)+y(j,k))$(ord(i) lt ord(j)) =g= 2*c(i,j,k)$(ord(i) lt ord(j));

*If both y(i,k) and y(j,k) is equal to 1 then c(i,j,k) has to be equal to 1 in
*order to satisfy the inequality.
condition2(i,j,k)..   (y(i,k)+y(j,k))$(ord(i) lt ord(j)) =l= 1 + c(i,j,k)$(ord(i) lt ord(j));

*The following conditions make sure that if the number of keys to nodes
*have in common is greater than or equal 3, then x(i,j) will be one. I used the
*notes on slide 11 from the first week where I set the  M=27 as an upper
*bound and epsilon=0,1.
condition3(i,j)..     sum(k, c(i,j,k))$(ord(i) lt ord(j)) - (27.1)*x(i,j)$(ord(i) lt ord(j)) =l= 3.1;

*If there is a connection between node i and j then x(i,j) is 1. Then the number
*of keys in common, c(i,j,k) has to be at least 3.
condition4(i,j)..     3*x(i,j)$(ord(i) lt ord(j)) =l= sum(k, c(i,j,k))$(ord(i) lt ord(j));

*I maximize the number of direct connections, i.e. the objective value z.
MODEL Problem2 /all/;
SOLVE Problem2 USING mip MAXIMIZING z;

DISPLAY z.l, x.l, y.l;


